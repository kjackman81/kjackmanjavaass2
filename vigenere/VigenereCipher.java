public class VigenereCipher
{

    private static final byte NUMBER_CHARS = 26;
    private static byte[][] vignereTable = new byte[NUMBER_CHARS][NUMBER_CHARS];

    // vignereTable is normalized to range [0 25]. A is 65
    private static final byte TRANSFORM_UPPER = 65; 

    // keyword is building block for key
    // key comprises repeating keyword such that 
    // the key length matches length of plaintext
    private String keyword;
    private String genKeyword = "";
    private String genCipher = "";
    private String genPlain = "";

    // the key comprising repeating keyword
    // key is normalized to range [0 25]
    private byte[] kEy;

    // the plain or message text
    private byte[] plainTexts;

    // the encrypted message text
    private byte[] cipherTexts;

    /**
     * Constructs a new VigenereCipher object,
     * Create table,
     * Prints table.
     */
    public VigenereCipher()
    {
        generateVigenereTable();
        printVigenereTable();
    }

    /**
     * Iterates threw two for loops,
     * To generate table.
     */
    public static void generateVigenereTable()
    {
        for (int i = 0; i < NUMBER_CHARS; i++)
        {
            for(int j = 0; j < NUMBER_CHARS; j++)
            {
                byte b = (byte)(TRANSFORM_UPPER + i + j);

                if(b > TRANSFORM_UPPER + NUMBER_CHARS-1)
                {
                    b = (byte)(b - NUMBER_CHARS);
                }
                vignereTable[i][j] = b;
                //vignereTable[i][j] = (byte)(65 + ((i + j) % NUMBER_CHARS));

            }
            System.out.println();
        }

    }

    /**
     * Use the keyword to generate a key.
     * Key length is the same as the length of the plain text.
     * The key comprises repeating keyword. 
     * The last keyword in the key is truncated if necessary.
     *
     * @param keyLength The number of characters in the key.
     * @param plainText The plain text for which a key will be generated.
     * @return The key for use in encrypting a message whose length matches the key length.
     */
    public String generateKey(String keyword, int keyLength)
    {
        int j = 0;
        kEy = new byte[keyLength];

        for(int i = 0; i < keyLength; i++)
        {
            kEy[i] = (byte)(keyword.charAt(j));
            j++;

            if(j == keyword.length())
            {
                j = 0;
            }
            
        }

        for(byte b : kEy)
        {
            genKeyword += (char)(b);  
        }

        return genKeyword;

    }

    /**
     * Encrypts plain text (message text) under the key using the Vigenere Table.
     * 
     * @param plainText The plain text (message text) to be encrypted
     * @param key The key under which the plain text is encrypted
     * @return The cipher text created by encrypting the plaintext under the generated key.
     */
    public String encrypt(String key, String plainText)
    {
        generateKey(key,plainText.length());
        cipherTexts = new byte[plainText.length()];
        plainTexts = plainText.getBytes();

        for(int i = 0; i < plainText.length(); i++)
        {
            cipherTexts[i] = (byte)((plainTexts[i] + kEy[i]) - TRANSFORM_UPPER);

            if(cipherTexts[i] >= TRANSFORM_UPPER + NUMBER_CHARS)
            {
                cipherTexts[i] = (byte)(cipherTexts[i] - NUMBER_CHARS);
            }

        }

        for(byte b : cipherTexts)
        {
            genCipher += (char)(b);  
        }

        return genCipher;

    }

    /**
     * Decrypts cipher text under the key using the Vigenere Table.
     * 
     * @param key The key under which the cipher text is deccrypted.
     * @param cipherText The cipher text to be decrypted.
     * @return The plain text obtained by decrypting the cipher text under the generated key.
     */

    public String decrypt(String keys, String cipherText)
    {
        cipherTexts = cipherText.getBytes();
        //plainText = new byte [cipherTexts.length()];

        for(int i = 0; i < cipherText.length(); i++)
        {
            plainTexts[i] = (byte)((cipherTexts[i]-kEy[i]) + TRANSFORM_UPPER);

            if(plainTexts[i] < TRANSFORM_UPPER)
            {
                plainTexts[i] = (byte)(plainTexts[i] + NUMBER_CHARS);
            }

        }

        for(byte b : plainTexts)
        {
            genPlain += (char)(b);  
        }

        return genPlain;
    }

    /**
     * Print the vigenere table.
     * 
     */
    public void printVigenereTable()
    {
        for (int i = 0; i < NUMBER_CHARS; i++)
        {
            for(int j = 0; j < NUMBER_CHARS; j++)
            {
                System.out.printf(" %c ",(char)vignereTable[i][j]);  
            }
            System.out.print("\n");
        }   

    }

    /**
     * Print the key.
     * 
     * @param key The key used for encryption and decryption.
     */
    public void printKey(String key)
    {
        System.out.println(key);
    }

    /**
     * Print the plain text (message text).
     * 
     * @param plainText The plain text (message text).
     */
    public void printMessage(String plainText)
    {
        System.out.println(plainText);
    }

    /**
     * Print the cipher text.
     * 
     * @param cipherText The encrypted message.
     */
    public void printCipher(String cipherText)
    {
        System.out.println(cipherText);

    }

    /**
     * Test method by: 
     * Generating and printing Vigenere Table,
     * Generating the key using the keyword,
     * Encrypting a message (plain text),
     * Decrypting the cipher text.
     * Printing the table, key, message, ciphertext & decrypted ciphertext.
     */
    public void testVigenere()
    {

        generateVigenereTable();

        String messageText = "MICHIGANTECHNOLOGICALUNIVERSITY";
        String key = generateKey("HOUGHTON", messageText.length());
        String cipherText = encrypt(key, messageText);
        String decrypted = decrypt(key, cipherText);

        printVigenereTable();
        printKey(key);
        printMessage(messageText);
        printCipher(cipherText);
        printMessage(decrypted);
    }
}