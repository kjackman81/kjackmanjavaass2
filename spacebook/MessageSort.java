import java.util.ArrayList;

public class MessageSort
{

    /**
     * Sorting algorithm used to sort messageText strings.
     * 
     * @param message Array. 
     */
    //Task 9
    public static void selectionSort(Message[] m)
    {

        for (int i = 0; i < m.length; i += 1)
            for (int j = i; j < m.length; j += 1)
                if (m[i].messageText.compareToIgnoreCase(m[j].messageText) > 0)
                {
                    swap(m, i, j);

                }  

    }

    /**
     * Used in conjunchion with the selectSort method to reasign the values.
     * 
     * @param message Array
     * @param int
     * @param int
     */
    private static void swap(Message[] arMsg, int to, int from)
    {
        String val = arMsg[to].messageText;
        arMsg[to].messageText = arMsg[from].messageText;
        arMsg[from].messageText = val;

    }

    /**
     * Sorting algorithm used to sort messageText strings.
     * 
     * @param message ArrayList 
     */
    //Task 10
    public static void selectionSort(ArrayList<Message> m)
    {
        for (int i = 0; i < m.size(); i += 1)
            for (int j = i; j < m.size(); j += 1)
                if (m.get(i).messageText.compareToIgnoreCase(m.get(j).messageText) > 0)
                {
                    swap(m, i, j);

                }  
    }

    /**
     * Used in conjunchion with the selectSort method to swap reasign values.
     * 
     * @param message ArrayList
     * @param int
     * @param int
     */
    private static void swap(ArrayList<Message> list, int to, int from)
    {
        String val = list.get(to).messageText;
        list.get(to).messageText = list.get(from).messageText;
        list.get(from).messageText = val;
    }

}
