//Question 4 Task 3
import java.util.ArrayList;

public class Group
{
    String groupName;
    String names = "";

    ArrayList<User> members;

    /**
     * Constructs a Group object,
     * Initalise the members ArrayList.
     * 
     * @param String
     */
    public Group(String groupName)
    {
        this.groupName = groupName;
        members = new ArrayList<User>();
    }

    /**
     * Add  a User to the member ArrayList.
     * 
     * @param User
     */
    public void addGroupMember(User user)
    {

        members.add(user);

    }

    /**
     * Send a message to the inbox of each group member.
     * 
     * @param Message
     */
    public void broadcastMessage(Message message)
    {
        for(User user : members)
        {
            user.inbox.add(message);
        }
    }

    /**
     * Use the \n between each name to ensure printed on separate lines,
     * All the group members names are printed.
     * 
     * @return String
     */
    private String groupList()
    {

        for(User user : members)
        {
            names += user.firstName + "\n";
        } 

        return names;
    }

    /**
     * Returns a string representation of the object.
     * 
     * @return String 
     */

    @Override
    public String toString() {
        return "GroupName = " + groupName + "\n" + groupList();
    } 
}
