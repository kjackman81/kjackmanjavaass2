
public class Friendship
{
    User sourceUser;
    User targetUser;
    /**
     * Constructs a friendship object,
     * Initilises the sourceUser, 
     * Initilises the targetUser.
     * 
     * @param User
     * @param User
     */

    public Friendship(User sourceUser, User targetUser)
    {
        this.sourceUser = sourceUser;
        this.targetUser = targetUser;
    }
}
