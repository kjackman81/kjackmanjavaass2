
/**
 * Enumeration class Enums - write a description of the enum class here
 * 
 * @author (kjackman:20029539)
 * @version (10.1.1)
 */

/**
 * Used to define collections of constants.
 */
public enum Status
{
    OFFLINE, ONLINE, BUSY
}

