
//Task 6
public class Message
{
    Subject   subject;
    String    messageText;
    User      from;
    User      to;
    /**
     * Constructs a message object,
     * Initilises Subject,
     * Initilises User,
     * Initilises User,
     * Initilises messageText.
     * 
     * @param Subject
     * @param User
     * @param User
     * @param String
     */

    public Message(Subject subject, User from, User to, String messageText)
    {
        // TODO initialize Subject field
        this.from           = from;
        this.to             = to;
        this.messageText    = messageText;
        this.subject        = subject;
    } 

    /**
     * Displays the contents of the message including to and from.
     */
    public void displayMessage()
    {
        String nameFrom = from.firstName;
        String nameTo   = to.firstName;
        System.out.println("MESSAGE subject " + subject + " " + nameFrom + " says \""+messageText + "\" to " + nameTo);
    }

    /**
     * Displays the contents of the messageText.
     */
    public void displayMessageContent()
    {
        System.out.println(messageText);
    }
}