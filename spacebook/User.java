
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class User
{
    String firstName;
    String lastName;
    int age;
    String nationality;
    String email;
    String password;

    Status status;

    ArrayList<Friendship> friendships   = new ArrayList<>();
    ArrayList<Message> inbox            = new ArrayList<>();
    ArrayList<Message> outbox           = new ArrayList<>(); 

    HashMap<String, Group> groups = new HashMap<>();

    /**
     * Constructs an object intended for testing only,
     * The firstName determined by caller,
     * Remaining fields used default data.
     * 
     * @param String
     */
    public User(String firstName)
    {
        setState(firstName, "Simpson", firstName+"@simpson.com", "secret");
    }

    /**
     * Constructs a User object,
     * Initalises parameters with setState method.
     * 
     * @param String
     * @param String
     * @param String
     * @param String 
     */

    public User(String firstName, String lastName, String email, String password)
    {
        setState(firstName, lastName, email, password);
    }  

    /**
     * Method used to initalise User constructor parameters, 
     * Initalise firstName,
     * Initalise lastName,
     * Initalise email,
     * Initalise password.
     * 
     * @param String
     * @param String
     * @param String
     * @param String 
     */

    public void setState(String firstName, String lastName, String email, String password)
    {
        this.firstName  = firstName;
        this.lastName   = lastName;
        this.email      = email;
        this.password   = password;
        this.status     = Status.ONLINE;
        // Task 2 initialize status

    } 

    /**
     * Initalises the Status, used by the User object.
     * 
     * @param Status
     */
    // Task 2
    public void setStatus(Status status)
    {
        this.status = status;
    }

    /**
     * Sends a message to the outbox of all members of friendships ArrayList,
     * Creates a new message object.
     * 
     * @param Subject
     * @param String
     * 
     */
    public void broadcastMessage(Subject subject, String messageText)
    {
        for(Friendship f : friendships)
        {
            Message message = new Message(subject, this, f.targetUser, messageText);
            outbox.add(message);
            f.targetUser.inbox.add(message);
        }
    }

    /**
     * Sends a message to its own outbox ArrayList,
     * Sends a message to  the recievers inbox ArrayList.
     * 
     * @param Message
     */
    // Task 3
    public void sendMessage(Message message)
    {
        outbox.add(message);
        message.to.inbox.add(message);
    }

    /**
     * Creates message object with parameters,
     * Sends a message to its own outbox ArrayList,
     * Sends a message to  the recievers inbox ArrayList.
     * 
     * @param String
     * @param User
     * @param Subject 
     */
    public void sendMessage(Subject subject, User to, String messageText)
    {
        Message message = new Message(subject,this, to, messageText);
        outbox.add(message);
        to.inbox.add(message);
    }

    /**
     * Calls the displayMessage method in the Message class, 
     * Displays the entire contents of each message in the outbox Arraylist.
     */
    public void displayOutbox()
    {
        for(Message msg : outbox)
        {
            msg.displayMessage();
        }
    }

    /**
     * Calls the displayMessage method in the Message class, 
     * Displays the entire contents of each message in the inbox Arraylist.
     */
    public void displayInbox()
    {
        for(Message msg : inbox)
        {
            msg.displayMessage();
        }
    }

    /**
     * Checks to see are you trying to create a friend object with yourself,
     * Checks to see are you trying to create a friend object with a person you already are friends with,
     * If not then create a new FriendShip object with the User parameter.
     * 
     * @param User
     */
    // Task 1
    public void befriend(User friend)
    {
        if(friend == this)
        {
            System.out.println("Opps! You seem to have made a mistake in attempting to befriend yourself");
        }
        else if(friendshipsContains(friend))
        {
            System.out.println("You attempted to befriend " + friend.firstName + " who is already a friend");
        }
        else
        {
            Friendship friendship = new Friendship(this, friend);
            friendships.add(friendship);
        }
    }

    /**
     * Loops througth the ArrayList,
     * Checks to see are you friends with a User already. 
     * 
     * @returns true if correct
     * @returns false if not already friends
     * @param User
     */
    // Task 1
    private boolean friendshipsContains(User friend)
    {
        for (Friendship f : friendships)
        {
            if(f.targetUser == friend)
                return true;
        }

        return false;

    }

    /**
     * Empties the Friendship ArrayList. 
     */

    public void unfriendAll()
    {
        friendships.clear();
    }

    /**
     * Loops througth the ArrayList,
     * If a particular User/friend is found,
     * It then removes this User.
     * 
     * @param User
     */
    public void unfriend(User friend) 
    {
        for(Friendship friendship : friendships)
        {
            if(friendship.targetUser == friend)
            {
                friendships.remove(friendship);
                return;
            }
        }
    }

    /**
     * Displays all Users in the Friendship ArrayList,
     * Prints there details,
     * If empty displays a message.
     */

    public void displayFriends() 
    {
        if(friendships.isEmpty())
        {
            System.out.println("Unfortunately you have no friends");
        }

        System.out.println("I'm " + this.firstName + " " + this.lastName + " and these are my \"friends\" hehe :-)");
       
        for(Friendship friendship : friendships)
        {
            System.out.println("My friend "+ friendship.targetUser.firstName + " is " + friendship.targetUser.status);
        }
    }

    /**
     * Finds all Users in the Friendship ArrayList based on there status,
     * Prints there details.
     * 
     * @param Status
     */
    // Task 2
    public void displayFriends(Status status) 
    {
        for(Friendship f: friendships)
        {
            if(f.targetUser.status == status)
                System.out.println(f.targetUser.status + ":  " + f.targetUser.firstName);
            //System.out.printf("%s:%s",f.targetUser.status,f.targetUser.firstName + "\n" );
        }
    }

    /**
     * Adds a group to the groups HashSet,
     * Uses the groupName parameter as the key,
     * Create a new group with that name.
     * 
     * @param String
     */

    public void addGroup(String groupName)
    {
        groups.put(groupName, new Group(groupName));
    }

    /**
     * Uses the groupName as the key to get the value,
     * Stores it in the group varible, as a reference,
     * Then adds the User as a new member.
     * 
     * @param String
     * @param User
     */

    public void addGroupMember(String groupName, User user)
    {
        Group group = groups.get(groupName);
        group.addGroupMember(user);
    }

    /**
     * Sends a message to the inbox ArrayList of each member in the group,
     * Calls on the broadcast method in the group class.
     * 
     * @param String
     * @param Message
     */
    public void broadcastMessage(String groupName, Message message)
    {
        groups.get(groupName).broadcastMessage(message);
    }

    // Task 7
    /**
     * Displays the content of the messages from the ArrayList of messages,
     * Calls the displayMessage method from the message class.
     * 
     * @param ArrayList<Message>.
     */
    public void displayMessages(ArrayList<Message> messages)
    {
        for (Message mess : messages)
        {
            mess.displayMessage();
        }
    }

    /**
     * Displays the content of a message filtered by its subject from the ArrayList of messages,
     * Calls the displayMessage method from the message class.
     * 
     * @param Subject
     * @param ArrayList<Message>
     */
    // Task 8
    public void displayMessages(Subject subject, ArrayList<Message> messages)
    {
        for (Message mess : messages)
        {
            if(mess.subject == subject)
            {
                mess.displayMessage();
            }
        }

    }

    /**
     * Search the message ArrayList for matching parameter content,
     * Returns the first Message containing content, else null.
     * 
     * @param  String  
     * @param  ArrayList<Message>       
     * @return Message        
     */
    public Message search(String content, ArrayList<Message> msg)
    {
        int index = 0;
        while(index < msg.size())
        {
            Message thisMsg = msg.get(index);
            if(content.equals(thisMsg.messageText))
            {
                return thisMsg;
            }
            index += 1;
        }
        return null;
    }

    /**
     * Search the message ArrayList for matching subject to the subject parameter,
     * Returns the first Message with subject matching param, else null.
     * 
     * @param  Subject   
     * @param  ArrayList<Message>  
     * @return Message        
     */
    public Message search(Subject subject, ArrayList<Message> msg)
    {
        int index = 0;
        while(index < msg.size())
        {
            Message thisMsg = msg.get(index);
            if(subject == thisMsg.subject)
            {
                return thisMsg;
            }
            index += 1;
        }
        return null;        
    }
}