
/**
 * Class contains personal details of person 
 * 
 * @author jf
 * @version 07.3.2016
 */
public class Person
{
    // Persons personal details
    private final String firstName;
    private final String lastName;
    String email;
    // A pin number associated with person
    private int pin;

    /**
     * Construct Person object,
     * Initalises its parameters.
     * 
     * @param firstName
     * @param lastName
     * @param email
     * @param pin
     */
    public Person(String firstName, String lastName, String email, int pin)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.pin = pin;
        // TODO 1 Complete initialization with validation of remaining fields
    }

    /**
     * Accessor to retrieve pin number
     * 
     * @return The pin number
     */
    public int getPin()
    {
        return pin;
    }

    /**
     * Generates a String representation of the Person object.
     * 
     * @return String
     */
    public String toString()
    {
        return "Full name: " + firstName + " " + lastName + " : Email : " + email;
    }
}
