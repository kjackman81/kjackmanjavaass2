import java.util.ArrayList;
/**
 * Write a description of class mergeSortBid here.
 * 
 * @author (kjackman:20029539) 
 * @version (10.1.0)
 */

public class MergeSortBid
{

    static ArrayList<Bid> aux;

    /**
     * Sorting algorithm merging the results with aid of auxiliary array.
     */
    public static void merge(ArrayList<Bid> a, int lo, int mid, int hi)
    {
        for (int k = lo; k <= hi; k++)
        {
            aux.set(k, a.get(k));
        }

        int i = lo;
        int j = mid + 1;
        for (int k = lo; k <= hi; k++)
        {
            if (i > mid)                                    a.set(k,aux.get(j++));
            else if (j > hi)                                a.set(k,aux.get(i++));
            else if (compare(aux.get(j) , aux.get(i)) < 0)  a.set(k,aux.get(j++));
            else                                            a.set(k,aux.get(i++));
        }
    }

    /**
     * Initialises the aux array,
     * Sorts the bids in succession to use the merge algorithm.
     * 
     * @param ArrayList<Bid>
     */
    public static void sort(ArrayList<Bid> a)
    {
        aux = new ArrayList<Bid>();
        for(int i = 0; i < a.size();i++)
        {
            aux.add(null);
        }
        int N = a.size();

        for (int size = 1; size < N; size = size + size)
        {
            for (int lo = 0; lo < N - size; lo += size + size)
            {
                merge(a, lo, lo + size - 1, Math.min(lo + size + size - 1, N - 1));
            }
        }
    } 

    /**
     * The bid amount values are being compared,
     * Returns the value for incrementing arrays.
     * 
     * @param  Bid
     * @param  Bid
     * @return int
     */
    public static int compare(Bid a,Bid b)
    {
        if (a.amountBid < b.amountBid)
            return -1;
        else if (a.amountBid == b.amountBid)
            return 0;
        else 
            return 1;
    }

    /**
     * Calls the toString method in the Bid object,
     * Prints its String repesentation.
     * 
     * @param ArrayList<Bid>
     */
    public static void print(ArrayList<Bid> a)
    {
        for (Bid b : a)
        {
            System.out.println(b);
        }
        System.out.println();
    }
}
