

/**
 * Class to create and manage an auction lot
 * 
 * @author jf
 * @version 07.3.2016
 *
 */
public class Lot
{
    // unique lot id
    final int lotId;
    // a description of the lot
    String description;

    /**
     * Construct a Lot object, 
     * Initializing its lotId number and description.
     * 
     * @param int 
     * @param String
     */
    public Lot(int lotId, String description)
    {
        this.lotId = lotId;
        this.description = description;
    }
    
    /**
     * Constructs a string representation of this object.
     * 
     * @return string
     */
    public String toString()
    {
        return "Lot id: " + lotId + ": " + description;
    }   
}
