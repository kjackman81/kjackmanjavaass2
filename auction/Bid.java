
/**
 * Class to manage bids
 * A bid comprises:
 * an item or lot being bid for
 * a bidder
 * an amount being bid or offered for the lot
 * 
 * @author jf
 * @version 07.3.2016
 */
public class Bid
{

    Lot lot;
    Person bidder;
    final int amountBid;

    /**
     * Constructs a Bid object,
     * Initalises its parameters lot, bidder and amountBid.
     * 
     * @param Lot 
     * @param Person 
     * @param int 
     */
    public Bid(Lot lot, Person bidder, int amountBid)
    {
        // TODO 1 Initialize all fields
        this.amountBid = amountBid;
        this.bidder = bidder;
        this.lot = lot;
    }

    /**
     * Accessor to retrieve the amountBid field.
     * 
     * @return int
     */
    public int getAmountBid()
    {
        return amountBid;
    }

    /**
     * Constructs a string representation of this object
     * 
     * @return string 
     */    
    public String toString()
    {
        // TODO 2 Implement this method
        return "lotiD: " + lot.lotId + " lot description: " + lot.description + " Bidder pin " + bidder.getPin() + " amount bid: " + getAmountBid()+ "\n";

    }

}
